//
//  AppDelegate.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

#define appDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) DataManager *dataManager;

@end

