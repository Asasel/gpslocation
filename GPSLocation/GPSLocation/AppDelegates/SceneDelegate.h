//
//  SceneDelegate.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

