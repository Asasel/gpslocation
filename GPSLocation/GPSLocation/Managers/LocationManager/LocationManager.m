//
//  LocationManager.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager {
    CLLocationManager *_locationManager;
    NSError *_lastLocationError;
}

#pragma mark - Constructor

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;    // the accuracy of up to 100 meters
    }
    return self;
}

# pragma mark - Functions

- (void)startUpdateLocations {
    [_locationManager requestAlwaysAuthorization];
    [_locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = locations.lastObject;
    if (self.firstLocation == nil) {
        [self updateLocation:newLocation];
        return;
    }
    NSTimeInterval locationAge = [newLocation.timestamp timeIntervalSinceDate:self.firstLocation.timestamp];
    if (locationAge < 30.0) {   // each 30 sec
        return;
    }
    [self updateLocation:newLocation];
}

- (void)updateLocation:(CLLocation *) newLocation {
    self.firstLocation = newLocation;
    self.hadleNewLocation(newLocation);
}

@end
