//
//  LocationManager.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property (strong,nonatomic) CLLocation *firstLocation;
@property (nonatomic, copy) void (^hadleNewLocation)(CLLocation *newLocation);

- (void)startUpdateLocations;

@end
