//
//  DataManager.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (strong, nonatomic, readonly) NSPersistentContainer *persistentContainer;
@property(strong, readonly) NSManagedObjectContext *mainContext;
 
- (instancetype)initWithCompletion:(void (^)(NSError *error)) completion;
- (NSArray *)fetchObjectsWithRequest:(NSFetchRequest *)request error: (NSError *)error;
- (void)saveContext;

@end
