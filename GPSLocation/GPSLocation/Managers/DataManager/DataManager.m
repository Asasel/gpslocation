//
//  DataManager.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import "DataManager.h"


@implementation DataManager

#pragma mark - Constructor

- (instancetype)initWithCompletion:(void (^)(NSError *error)) completion {
    self = [super init];
    if (self) {
        _persistentContainer = [self createPersistentContainerWithCompletionBlock:^(NSError *error) {
            completion(error);
        }];
        _mainContext = _persistentContainer.viewContext;
    }
    return self;
}

#pragma mark - Core Data stack

- (NSPersistentContainer *)createPersistentContainerWithCompletionBlock:(void (^)(NSError *error)) completion {
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"GPSLocation"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription,
                                                                              NSError *error) {
                completion(error);
            }];
        }
    }
    return _persistentContainer;
}

- (NSArray *)fetchObjectsWithRequest:(NSFetchRequest *)request error:(NSError *)error {
    return [_persistentContainer.newBackgroundContext executeFetchRequest:request error:&error];
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
