//
//  Location+CoreDataClass.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

@interface Location : NSManagedObject

+ (instancetype)insertNewObjectInContext:(NSManagedObjectContext *)context;

@end

#import "Location+CoreDataProperties.h"
