//
//  Location+CoreDataClass.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//
//

#import "Location+CoreDataClass.h"

@implementation Location

+ (instancetype)insertNewObjectInContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
}

- (void) awakeFromInsert {
    self.date = [NSDate date];
    self.latitude = 0.0;
    self.longtitude = 0.0;
}

@end
