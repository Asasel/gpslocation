//
//  Location+CoreDataProperties.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//
//

#import "Location+CoreDataClass.h"


@interface Location (CoreDataProperties)

+ (NSFetchRequest<Location *> *)fetchRequest;

@property (nonatomic) double latitude;
@property (nonatomic) double longtitude;
@property (nullable, nonatomic, copy) NSDate *date;

@end
