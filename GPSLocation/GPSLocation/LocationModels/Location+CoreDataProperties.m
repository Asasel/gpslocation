//
//  Location+CoreDataProperties.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//
//

#import "Location+CoreDataProperties.h"

@implementation Location (CoreDataProperties)

+ (NSFetchRequest<Location *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Location"];
}

@dynamic latitude;
@dynamic longtitude;
@dynamic date;

@end
