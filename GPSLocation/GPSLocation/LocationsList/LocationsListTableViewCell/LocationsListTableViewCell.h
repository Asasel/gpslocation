//
//  LocationsListTableViewCell.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationsListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longtitudeLabel;

@end

