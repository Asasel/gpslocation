//
//  LocationsListViewModel.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"
#import "LocationsListDatasource.h"
#import "AppDelegate.h"
#import "Location+CoreDataClass.h"

@interface LocationsListViewModel : NSObject

@property (nonatomic, readonly) LocationManager *locationManager;
@property (nonatomic, readonly) LocationsListDatasource *dataSource;

- (void)configureLocationManager;
- (void)createDataSourceForTableView:(UITableView *) tableView;

@end
