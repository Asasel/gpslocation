//
//  LocationsListViewModel.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import "LocationsListViewModel.h"

@implementation LocationsListViewModel

#pragma mark - Constructor

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [LocationManager new];
    }
    return self;
}

#pragma mark - Location Manager

- (void)configureLocationManager {
    [self.locationManager startUpdateLocations];
    __weak __typeof__(self) weakSelf = self;
    self.locationManager.hadleNewLocation = ^(CLLocation *newLocation) {
        NSLog(@"\nhadleNewLocation(): newLocation = %@\n", newLocation);
        __typeof__(self) strongSelf = weakSelf;
        [strongSelf storeNewLocation:newLocation];
    };
}

- (void)createDataSourceForTableView:(UITableView *) tableView; {
    _dataSource = [[LocationsListDatasource alloc] initWithTableView:tableView
                                                         dataManager:appDelegate.dataManager];
}

- (void)storeNewLocation:(CLLocation *)newLocation {
    CLLocationCoordinate2D coordinate = newLocation.coordinate;
    NSManagedObjectContext *context = appDelegate.dataManager.mainContext;
    Location *location = [Location insertNewObjectInContext:context];
    location.latitude = coordinate.latitude;
    location.longtitude = coordinate.longitude;
    [appDelegate.dataManager saveContext];
}

@end
