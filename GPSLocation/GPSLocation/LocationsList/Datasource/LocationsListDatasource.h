//
//  LocationsListDatasource.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DataManager.h"
#import "LocationsListTableViewCell.h"
#import "Location+CoreDataClass.h"

@interface LocationsListDatasource : NSObject <UITableViewDataSource, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic, readonly) DataManager *dataManager;
@property (nonatomic) UITableView *tableView;

- (instancetype)initWithTableView:(UITableView *) tableView dataManager:(DataManager *)dataManager;

@end
