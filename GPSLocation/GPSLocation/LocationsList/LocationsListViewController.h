//
//  LocationsListViewController.h
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationsListTableViewCell.h"
#import "LocationsListViewModel.h"

@interface LocationsListViewController : UIViewController

#pragma mark - Properties

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) LocationsListViewModel *viewModel;

@end

