//
//  LocationsListViewController.m
//  GPSLocation
//
//  Created by Angelina Latash on 6/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

#import "LocationsListViewController.h"

@interface LocationsListViewController ()
- (void)configureTableView;
@end

@implementation LocationsListViewController

#pragma mark - View Controller Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    self.viewModel = [LocationsListViewModel new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTableView];
    [self.viewModel configureLocationManager];
}

#pragma mark - Configure Table View

- (void)configureTableView {
    [self.viewModel createDataSourceForTableView:self.tableView];
    self.tableView.dataSource = self.viewModel.dataSource;
    self.tableView.tableFooterView = [UIView new];
}

@end
